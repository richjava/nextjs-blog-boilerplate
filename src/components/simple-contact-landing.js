const SimpleContactLanding = () => {
  return (
    <section className="contact-landing simple-contact-landing">
     <p>Here is a blurb for the Contact page landing section that will come from the CMS.</p>
      <style jsx>{`
      .simple-contact-landing {
        height:500px;
      }
      `}</style>
    </section>
  );
};

export default SimpleContactLanding;
