import Link from 'next/link';

const SimpleBlogTeaser = () => {
  return (
    <section className="blog-teaser">
      <p>
        Here is a blurb for the BlogTeaser component that will come from the
        CMS.
      </p>
      <Link href="/blog">
        <a>Go to Blog</a>
      </Link>
      <style jsx>{`
        .blog-teaser {
          background: #ccc;
        }
      `}</style>
    </section>
  );
};

export default SimpleBlogTeaser;
