import React from 'react';
import Link from 'next/link';
import Head from 'next/head';

import { withRouter } from 'next/router';


const Page = () => (
    <>
        <Head>
            <title>Home</title>
        </Head>
        <main>
            <Link href="#">
                <a>GET STARTED</a>
            </Link>
            <Link href="/sites">
                <a>START WITH A SITE</a>
            </Link>
        </main>

    </>
);

export default withRouter(Page);
