import { useState, useEffect } from 'react';
import Head from 'next/head';
import dynamic from 'next/dynamic';
import { withRouter } from 'next/router';
import { parseGlobalStyles } from '../../utils/siteUtils';

const fetchComponentData = async () => {
  return {
    displayName: 'Contact landing',
    name: 'contactLanding',
    slug: 'contact-landing',
    rank: 'U',
    description: 'Description for simple contact landing cmp',
    currentTemplate: {
      displayName: 'Simple',
      name: 'simpleContactLanding',
      slug: 'simple-contact-landing',
      fileName: 'simple-contact-landing',
      cmpName: 'SimpleContactLanding',
      pageFileName: 'contact',
      repoUrl: ''
    }
  };
};
const fetchTemplateData = () => {
  return {
    displayName: 'Simple',
    slug: 'simple-contact-landing',
    fileName: 'simple-contact-landing',
    name: 'simpleContactLanding',
    cmpName: 'SimpleContactLanding',
    pageFileName: 'contact',
    repoUrl: ''
  };
};

const Page = ({ data, defaultGlobalStyles, defaultComponentStyles }) => {
  const INITIAL_STATE = {
    background: 'red'
  };

  const [colors, setColors] = useState(INITIAL_STATE);
  const [style, setStyle] = useState({});
  const [globalStyle, setGlobalStyle] = useState({});
  const [component, setComponent] = useState(data);
  const [src, setSrc] = useState(
    `http://localhost:3000/sites/wedding/use-cases/make-contact/template?template=simple-contact-landing`
  );

  let templ = fetchTemplateData();
  let Template = dynamic(() => import(`../../../../components/${templ.slug}`));

  /**
   * On load, get styles from local storage
   */
  useEffect(() => {
    init();
  }, []);

  function init(){

    setCustomComponentStyle();
    setCustomGlobalStyle();
  }

  function setCustomGlobalStyle() {
    var globalStyles = localStorage.getItem('customGlobalStyles');
    if (globalStyles) {
      setGlobalStyle(JSON.parse(globalStyles));
    } else {
      // style[component.name] = {
      //   ...style[component.name],
      //   ...newStyle[component.name]
      // };
      setGlobalStyle(defaultGlobalStyles);
    }
  }

  function setCustomComponentStyle() {
    var localCustomComponentStyles = JSON.parse(localStorage.getItem(
      'customComponentStyles'
    ));
    if (localCustomComponentStyles && localCustomComponentStyles[component.name]) {
      setStyle(localCustomComponentStyles);
    } else {
      style[component.name] = {};
      // setStyle(defaultComponentStyles);
    }
  }

  /**
   * When style is changed, apply styles to iframe
   */
  // useEffect(() => {
  //   applyStyle();
  // }, [style]);

  /**
   * When a new component is navigated to, update the iframe src
   */
  useEffect(() => {
    init();
    setSrc(
      `http://localhost:3000/sites/wedding/use-cases/make-contact/template?template=${component.currentTemplate.slug}`
    );
  }, [component]);

  async function displayComponent(slug) {
    //TODO get from db
    let component;
    if (slug === 'home-landing') {
      component = {
        displayName: 'Home landing',
        name: 'homeLanding',
        slug: 'home-landing',
        rank: 'U',
        description: 'Description for simple contact landing cmp',
        currentTemplate: {
          displayName: 'Simple',
          name: 'simpleHomeLanding',
          slug: 'simple-home-landing',
          fileName: 'simple-home-landing',
          cmpName: 'SimpleHomeLanding',
          pageFileName: 'home',
          repoUrl: ''
        }
      };
      component = component;
    } else {
      component = await fetchComponentData();
    }
    setComponent(component);
  }

  function addStyle(cssStyles, id) {
    let head = document.getElementById('tmplIf').contentWindow.document.head;

    // dynamically add component styles
    var css = document.createElement('style');
    css.type = 'text/css';
    css.id = id;
    css.appendChild(document.createTextNode(cssStyles));
    let existingStyle = document
      .getElementById('tmplIf')
      .contentWindow.document.getElementById(id);
    if (existingStyle) {
      existingStyle.remove();
    }
    head.append(css);
  }

  function parseStyles(styles) {
    let parsedStyles = '\n';
    for (const cmpProp in styles) {
      if (cmpProp !== component.name)
        switch (cmpProp) {
          case 'classes':
            for (const classProp in styles[cmpProp]) {
              parsedStyles += `.${classProp.replace(
                /[A-Z]/g,
                m => '-' + m.toLowerCase()
              )} {\n`;
              for (const cssProp in styles[cmpProp][classProp]) {
                parsedStyles += `\t${cssProp}: ${styles[cmpProp][classProp][cssProp]};\n`;
              }
              parsedStyles += `}\n`;
            }
            break;
          case 'tags':
            for (const tagProp in styles[cmpProp]) {
              parsedStyles += `${tagProp} {\n`;
              for (const cssProp in styles[cmpProp][tagProp]) {
                parsedStyles += `\t${cssProp}: ${styles[cmpProp][tagProp][cssProp]};\n`;
              }
              parsedStyles += `\n}\n`;
            }
            break;
          default:
            return;
        }
    }
    return parsedStyles;
  }

  function applyStyle(isWait) {
    if (style && style[component.name]) {
      let head = document.getElementById('tmplIf').contentWindow.document.head;

      // dynamically add bootstrap library
      var bsLink = document.createElement('link');
      bsLink.type = 'text/css';
      bsLink.rel = 'stylesheet';
      bsLink.href =
        'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css';
      head.append(bsLink);

      let componentStyles = parseStyles(style[component.name]);
      let customGlobalStyles = parseGlobalStyles(globalStyle);
      // update locally stored style config
      localStorage.setItem('customComponentStyles', JSON.stringify(style));

      if (!isWait) {
        addStyle(customGlobalStyles, 'customGlobalStyles');
        addStyle(componentStyles, 'customTemplateStyles');
        return;
      }
      //WORKAROUND allow time for iframe to load
      setTimeout(() => {
        addStyle(customGlobalStyles, 'customGlobalStyles');
        addStyle(componentStyles, 'customTemplateStyles');
      }, 1000);

      
    }
  }

  function handleBackgroundColor(event) {
    const { value } = event.target;
    let newStyle = {};
    newStyle[component.name] = {
      classes: {}
    };
    newStyle[component.name].classes[component.name] = {
      background: value
    };
    style[component.name] = {
      ...style[component.name],
      ...newStyle[component.name]
    };
    applyStyle(false);
  }

  function handleBodyTextColor(event) {
    const { value } = event.target;
    let newStyle = {};
    newStyle[component.name] = {
      tags: {}
    };
    newStyle[component.name].tags.p = {
      color: value
    };
    style[component.name] = {
      ...style[component.name],
      ...newStyle[component.name]
    };
    applyStyle(false);
  }

  return (
    <>
      <Head>
        <title>Make contact</title>
      </Head>
      <main>
        <div className="toolbar">
          <div>
            <h4>
              Wedding <span style={{ fontSize: '10px' }}>pencil icon</span>
            </h4>
            <div>
              <span>2 use-cases</span>
              <span>5 pages</span>
              <span>5 components</span>
            </div>
          </div>
          <div className="toolbar-btns">
            <button>Create site</button>
          </div>
        </div>
        <div>
          <h5>
            <span style={{ fontSize: '10px' }}>back icon</span>Make contact
          </h5>
        </div>
        <div className="content">
          <div className="side-nav">
            <h2>Architecture</h2>
            <h6>Home page</h6>
            <ul>
              <li onClick={() => displayComponent('home-landing')}>
                Home landing
              </li>
            </ul>
            <h6>Contact page</h6>
            <ul>
              <li onClick={() => displayComponent('contact-landing')}>
                Contact landing
              </li>
            </ul>
          </div>
          <div className="component">
            <div className="cmp-item">
              <h3>
                {component.displayName}{' '}
                <span style={{ fontSize: '14px' }}>
                  {component.currentTemplate.displayName}
                </span>
              </h3>
              <p>{component.description}</p>
              <iframe
                id="tmplIf"
                className="tmpl-if"
                frameBorder="0"
                width="100%"
                height="100%"
                onLoad={() => {
                  applyStyle();
                }}
                src={src}
              ></iframe>
            </div>
          </div>
          {style[component.name] && (
            <div className="design">
              <h2>Design</h2>
              <h5>Background</h5>
              <h6>Color</h6>
              None | Primary | Secondary | Tertiary | Dark | Custom
              <input
                className=""
                type="color"
                defaultValue={
                  style[component.name] &&
                  style[component.name].classes &&
                  style[component.name].classes[component.name] ?
                  style[component.name].classes[component.name].background :
                  globalStyle.variables.colors.darkContrastColor
                }
                onChange={e => handleBackgroundColor(e)}
              />
              <h6>Pattern</h6>
              <p>patterns go here</p>
              <h5>Text colors</h5>
              <h6>Body</h6>
              Primary | Secondary | Tertiary | Dark | Custom
              <input
                className=""
                type="color"
                defaultValue={
                  style[component.name] &&
                  style[component.name].tags &&
                  style[component.name].tags.p ?
                  style[component.name].tags.p.color :
                  globalStyle.variables.colors.darkColor
                }
                onChange={e => handleBodyTextColor(e)}
              />
            </div>
          )}
        </div>
      </main>
      <style jsx>{`
        .toolbar {
          display: flex;
          margin-bottom: 50px;
        }
        .toolbar-btns {
          margin-left: 500px;
        }
        .content {
          display: flex;
        }
        .side-nav {
          width: 25%;
        }
        .component {
          width: 50%;
        }
        .cmp-item {
          cursor: pointer;
          margin-bottom: 20px;
          height: calc(100vh - 200px);
        }
        .design {
          width: 25%;
          padding: 0 10px;
        }
      `}</style>
    </>
  );
};

export async function getServerSideProps() {
  const component = await fetchComponentData();
  return {
    props: {
      data: component,
      defaultGlobalStyles: {
        variables: {
          colors: {
            primaryColor: '#474677',
            primaryContrastColor: '#ffffff',
            secondaryColor: '#7f98f0',
            secondaryContrastColor: '#ffffff',
            tertiaryColor: '#8fc8fa',
            tertiaryContrastColor: '#ffffff',
            darkColor: '#353546',
            darkContrastColor: '#ffffff'
          },
        },
        classes: {
          btnPrimary: {
            backgroundColor: 'var(--secondary-color)'
          }
        },       
        
        
        
      }
      // defaultComponentStyles: {
      //   //style[component.name].tags.p.color
      //   contactLanding: {
      //     tags: {
      //       p: {
      //         color: 'green'
      //       }
      //     }
      //   }
      // }
    } //data,
  };
}

export default withRouter(Page);
