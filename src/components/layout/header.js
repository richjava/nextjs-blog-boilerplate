import Link from 'next/link';

const Header = () => {
  return (
    <header className="o-header">
      <p>
        Site &nbsp;{' '}
        <Link href="/">
          <a>Home</a>
        </Link>{' '}
        &nbsp;{' '}
        <Link href="/blog">
          <a>Blog</a>
        </Link>
      </p>
      <hr />
    </header>
  );
};

export default Header;
