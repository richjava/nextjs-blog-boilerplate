import Link from 'next/link';

const SimpleHomeLanding = () => {
  return (
    <section className="home-landing simple-home-landing">
     <p>Here is a blurb for the Home page landing section that will come from the CMS.</p>
     <Link href="/contact">
        <a>Get in touch</a>
      </Link>
      <button className="btn btn-primary">Test bootstrap btn</button>
      <style jsx>{`
      
      `}</style>
    </section>
  );
};

export default SimpleHomeLanding;
