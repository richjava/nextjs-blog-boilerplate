let useCases = {
  "useCases": [
    {
      "displayName": "View blog",
      "slug": "view-blog",
      "pages": [{
          "displayName": "Simple blog list",
          "slug": "simple-blog-list",
          "title": "Blog",
          "fileName": "blog",
          "path": "/",
          "depth": 0,
          "description": "",
          "imageUrl": "",
          "repoUrl": ""
        },
        {
          "displayName": "Simple blog detail",
          "slug": "simple-blog-detail",
          "titleProp": "title",
          "fileName": "[slug].js",
          "path": "/posts",
          "depth": 1,
          "description": "",
          "imageUrl": "",
          "repoUrl": ""
        }
      ],
      "components": [
        {
          "displayName": "Simple blog teaser",
          "slug": "simple-blog-teaser",
          "fileName": "simple-blog-teaser",
          "componentName": "SimpleBlogTeaser",
          "pageFileName": "index",
          "rank": "l",
          "description": "",
          "imageUrl": "",
          "repoUrl": ""
        }
      ]
    },
    {
        "displayName": "Make contact",
        "slug": "make-contact",
        "components": [
          {
            "displayName": "Simple contact landing section",
            "slug": "simple-contact-landing-section",
            "fileName": "simple-contact-landing-section",
            "componentName": "SimpleContactLandingSection",
            "pageFileName": "index",
            "rank": "U",
            "description": "",
            "imageUrl": "",
            "repoUrl": ""
          }
        ]
      }
  ]
}

export default useCases;