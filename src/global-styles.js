import { createGlobalStyle } from 'styled-components';
import theme from './theme';

const GlobalStyles = createGlobalStyle`
  // global css variables
  html {
    --color-primary: ${theme.primaryColor};
    --color-primary-contrast: ${theme.colorPrimaryContrast};
    --color-secondary: ${theme.colorSecondary};
    --color-secondary-contrast: ${theme.colorSecondaryContrast};
    --color-background: ${theme.colorBackground};
    --color-background-contrast: ${theme.colorBackgroundContrast};
  }

  //other global styles go here...
`;

export default GlobalStyles;