import React from 'react';
import Link from 'next/link';
import Head from 'next/head';

import { withRouter } from 'next/router';


const Page = () => (
    <>
        <Head>
            <title>Home</title>
        </Head>
        <main>
            <div className="toolbar">
                <h2>Sites</h2>
                <div className="toolbar-btns">
                    <button>Start from blank</button>
                </div>
            </div>
            <div style={{display: "flex"}}>
                <div className="theme-item">
                    <h5>Wedding</h5>
                    <br/>
                    <Link href="/sites/wedding">
                        <a>Demo</a>
                    </Link>
                    &nbsp;&nbsp;&nbsp;
                    <Link href="/sites/wedding">
                        <a>Create a site</a>
                    </Link>
                </div>
                <div className="theme-item">
                    <h5>Charity</h5>
                    <br/>
                    <Link href="/sites/wedding">
                        <a>Demo</a>
                    </Link>
                    &nbsp;&nbsp;&nbsp;
                    <Link href="/sites/wedding">
                        <a>Create a site</a>
                    </Link>
                </div>
            </div>

        </main>
        <style jsx>{`
      .toolbar {
        display:flex;
      }
      .theme-item {
          width:300px;
          height: 350px;
          border: 1px solid #ccc;
      }
    `}</style>
    </>
);

export default withRouter(Page);
