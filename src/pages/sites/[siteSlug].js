import { useState, useEffect, useRef } from 'react';

import Head from 'next/head';
import Link from 'next/link';

import { withRouter, useRouter } from 'next/router';
import { loadGetInitialProps } from 'next/dist/next-server/lib/utils';

import { parseGlobalStyles } from '../sites/utils/siteUtils';

const Page = ({ useCases, defaultGlobalStyles }) => {
  const router = useRouter();

  const [style, setStyle] = useState({});

  let inputs = {
    primaryColor: useRef(null),
    primaryContrastColor: useRef(null),
    secondaryColor: useRef(null),
    secondaryContrastColor: useRef(null),
    tertiaryColor: useRef(null),
    tertiaryContrastColor: useRef(null),
    darkColor: useRef(null),
    darkContrastColor: useRef(null)
  };

  // function to trigger our input file click
  // const uploadClick = e => {
  //   e.preventDefault();
  //   inputFile.click();
  //   return false;
  // };

  useEffect(() => {
    init();
    var localCustomGlobalStyles = localStorage.getItem('customGlobalStyles');
    if (localCustomGlobalStyles) {
      setStyle(JSON.parse(localCustomGlobalStyles));
    } else {
      setStyle(defaultGlobalStyles);
    }
  }, []);

  function init() {
    var eventMethod = window.addEventListener
      ? 'addEventListener'
      : 'attachEvent';
    var eventer = window[eventMethod];
    var messageEvent = eventMethod === 'attachEvent' ? 'onmessage' : 'message';

    eventer(messageEvent, function(e) {
      console.log(inputs[`${e.data}`]);
      if (inputs[`${e.data}`]) {
        inputs[`${e.data}`].current.click();
      }
    });
  }

  function addStyle(cssStyles) {
    let head = document.getElementById('tmplIf').contentWindow.document.head;

    // dynamically add component styles
    var css = document.createElement('style');
    css.type = 'text/css';
    css.id = 'customGlobalStyles';
    css.appendChild(document.createTextNode(cssStyles));
    let existingStyle = document
      .getElementById('tmplIf')
      .contentWindow.document.getElementById('customGlobalStyles');
    if (existingStyle) {
      existingStyle.remove();
    }
    head.append(css);
  }

  // function parseStyles() {
  //   let parsedStyles = '\n:root {\n';
  //   for (const type in style) {
  //     for (const prop in style[type]) {
  //       parsedStyles += `--${prop.replace(
  //         /[A-Z]/g,
  //         m => '-' + m.toLowerCase()
  //       )}: ${style[type][prop]};\n`;
  //     }
  //   }
  //   parsedStyles += '\n}';
  //   return parsedStyles;
  // }

  function applyStyle() {
    // dynamically add bootstrap library
    let head = document.getElementById('tmplIf').contentWindow.document.head;
    var bsLink = document.createElement('link');
    bsLink.type = 'text/css';
    bsLink.rel = 'stylesheet';
    bsLink.href =
      'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css';
    head.append(bsLink);

    // add global styles
    let customGlobalStyles = parseGlobalStyles(style);
    addStyle(customGlobalStyles);
debugger
    // update locally stored style config
    if (style && Object.entries(style).length > 0) {
      localStorage.setItem('customGlobalStyles', JSON.stringify(style));
    }
  }

  function handleCustomColor(colorName, event) {
    const { value } = event.target;
    let colors =  {} ;
    colors[`${colorName}Color`] = value;
    style.variables.colors = {
      ...style.variables.colors,
      ...colors
    };
    // update locally stored style config
    localStorage.setItem('customGlobalStyles', JSON.stringify(style));

    applyStyle();
  }

  return (
    <>
      <Head>
        <title>Wedding Site</title>
      </Head>
      <main>
        <div className="toolbar">
          <div>
            <h2>
              Wedding <span style={{ fontSize: '10px' }}>pencil icon</span>
            </h2>
            <div>
              <span>5 pages</span>
              <span>5 components</span>
            </div>
          </div>
          <div className="toolbar-btns">
            <button>Create site</button>
          </div>
        </div>
        <div className="content">
          <div style={{ width: '25%' }}>
            <div className="use-cases-toolbar">
              <h2>Use cases</h2>
              <button>Add use-cases</button>
            </div>
            {useCases.map(useCase => {
              let noOfPages = useCase.pages.filter(page => page.isCorePage)
                .length;
              return (
                <Link
                  key={useCase.slug}
                  href={`${router.query.siteSlug}/use-cases/${useCase.slug}`}
                >
                  <div className="use-case">
                    <h3>{useCase.displayName}</h3>
                    <p>{useCase.description}</p>
                    <div>
                      {noOfPages} pages
                      {useCase.pageComponents.length} components
                    </div>
                  </div>
                </Link>
              );
            })}
          </div>
          <div style={{ width: '75%' }}>
            <h2>Global styles</h2>
            <div className="global-styles">
              <div className="style-tile">
                <iframe
                  id="tmplIf"
                  className="tmpl-if"
                  frameBorder="0"
                  width="100%"
                  height="100%"
                  onLoad={() => {
                    applyStyle();
                  }}
                  src="http://localhost:3000/sites/wedding/template"
                ></iframe>
              </div>
              <div className="design">
                <h3>Design</h3>
                {style.variables && style.variables.colors && (
                  <div>
                    <h5>Colors</h5>
                    Primary{' '}
                    <input
                      className=""
                      type="color"
                      ref={inputs.primaryColor}
                      defaultValue={style.variables.colors.primaryColor}
                      onChange={e => handleCustomColor('primary', e)}
                    />{' '}
                    Contrast{' '}
                    <input
                      className=""
                      type="color"
                      ref={inputs.primaryContrastColor}
                      defaultValue={style.variables.colors.primaryContrastColor}
                      onChange={e => handleCustomColor('primaryContrast', e)}
                    />
                    <br />
                    Secondary{' '}
                    <input
                      className=""
                      type="color"
                      ref={inputs.secondaryColor}
                      defaultValue={style.variables.colors.secondaryColor}
                      onChange={e => handleCustomColor('secondary', e)}
                    />{' '}
                    Contrast{' '}
                    <input
                      className=""
                      type="color"
                      ref={inputs.secondaryContrastColor}
                      defaultValue={style.variables.colors.secondaryContrastColor}
                      onChange={e =>
                        handleCustomColor('secondaryContrast', e)
                      }
                    />
                    <br />
                    Tertiary{' '}
                    <input
                      className=""
                      type="color"
                      ref={inputs.tertiaryColor}
                      defaultValue={style.variables.colors.tertiaryColor}
                      onChange={e => handleCustomColor('tertiaryColor', e)}
                    />{' '}
                    Contrast{' '}
                    <input
                      className=""
                      type="color"
                      ref={inputs.tertiaryContrastColor}
                      defaultValue={style.variables.colors.tertiaryContrastColor}
                      onChange={e => handleCustomColor('tertiaryContrast', e)}
                    />
                    <br />
                    Dark{' '}
                    <input
                      className=""
                      type="color"
                      ref={inputs.darkColor}
                      defaultValue={style.variables.colors.darkColor}
                      onChange={e => handleCustomColor('dark', e)}
                    />{' '}
                    Contrast{' '}
                    <input
                      className=""
                      type="color"
                      ref={inputs.darkContrastColor}
                      defaultValue={style.variables.colors.darkContrastColor}
                      onChange={e => handleCustomColor('darkContrast', e)}
                    />
                  </div>
                )}
                <br />
                <br />
                <h5>Typography</h5>
                <h6>Primary font</h6>
                Name | Color
                <h6>Secondary font</h6>
                Name | Color
                <h6>Body font</h6>
                Name | Color
                <br />
                <br />
                <h5>Patterns</h5>
                <p>patterns go here</p>
              </div>
            </div>
          </div>
        </div>
      </main>
      <style jsx>{`
        .toolbar {
          display: flex;
          margin-bottom: 50px;
        }
        .toolbar-btns {
          margin-left: 500px;
        }
        .use-cases-toolbar {
          display: flex;
        }
        .content {
          display: flex;
          height: calc(100vh - 200px);
        }
        .global-styles {
          display: flex;
          width: 75%;
          height: 100%;
        }
        .style-tile {
          width: 70%;
          border: 1px solid #ccc;
        }

        .design {
          padding-left: 20px;
        }

        .use-case {
          margin: 20px;
          cursor: pointer;
          border: 1px solid #ccc;
        }
      `}</style>
    </>
  );
};

export async function getServerSideProps() {
  return {
    props: {
      defaultGlobalStyles: {
        variables: {
          colors: {
            primaryColor: '#474677',
            primaryContrastColor: '#ffffff',
            secondaryColor: '#7f98f0',
            secondaryContrastColor: '#ffffff',
            tertiaryColor: '#8fc8fa',
            tertiaryContrastColor: '#ffffff',
            darkColor: '#353546',
            darkContrastColor: '#ffffff'
          },
        },
        classes: {
          btnPrimary: {
            backgroundColor: 'var(--secondary-color)'
          }
        },       
      },
      useCases: [
        {
          displayName: 'Make contact',
          slug: 'make-contact',
          pages: [
            {
              displayName: 'Home',
              name: 'home',
              slug: 'home',
              title: 'Home',
              fileName: 'index',
              isCorePage: false,
              path: '/',
              depth: 0,
              description: '',
              imageUrl: '',
              repoUrl: ''
            },
            {
              displayName: 'Contact',
              name: 'contact',
              slug: 'contact',
              title: 'Contact',
              fileName: 'contact',
              isCorePage: true,
              path: '/',
              depth: 0,
              description: '',
              imageUrl: '',
              repoUrl: ''
            }
          ],
          pageComponents: [
            {
              displayName: 'Simple home landing',
              slug: 'simple-home-landing',
              fileName: 'simple-home-landing',
              cmpName: 'SimpleHomeLanding',
              pageFileName: 'index',
              rank: 'U',
              description: '',
              imageUrl: '',
              repoUrl: ''
            },
            {
              displayName: 'Simple contact landing',
              slug: 'simple-contact-landing',
              fileName: 'simple-contact-landing',
              cmpName: 'SimpleContactLanding',
              pageFileName: 'contact',
              rank: 'U',
              description: '',
              imageUrl: '',
              repoUrl: ''
            }
          ]
        },
        {
          displayName: 'View blog',
          slug: 'view-blog',
          pages: [
            {
              displayName: 'Home',
              slug: 'home',
              name: 'home',
              title: 'Home',
              fileName: 'index',
              isCorePage: false,
              path: '/',
              depth: 0,
              description: '',
              imageUrl: '',
              repoUrl: ''
            },
            {
              displayName: 'Simple blog',
              slug: 'simple-blog',
              title: 'Blog',
              name: 'simpleBlog',
              fileName: 'blog',
              isCorePage: true,
              path: '/',
              depth: 0,
              description: '',
              imageUrl: '',
              repoUrl: ''
            },
            {
              displayName: 'Simple blog detail',
              slug: 'simple-blog-detail',
              name: 'simpleBlogDetail',
              fileName: '[slug]',
              path: '/posts/',
              isCorePage: true,
              depth: 1,
              description: '',
              imageUrl: '',
              repoUrl: '',
              isDetail: true
            }
          ],
          pageComponents: [
            {
              displayName: 'Simple blog teaser',
              slug: 'simple-blog-teaser',
              fileName: 'simple-blog-teaser',
              cmpName: 'SimpleBlogTeaser',
              pageFileName: 'index',
              rank: 'l',
              description: '',
              imageUrl: '',
              repoUrl: ''
            },
            {
              displayName: 'Simple blog landing',
              slug: 'simple-blog-landing',
              fileName: 'simple-blog-landing',
              cmpName: 'SimpleBlogLanding',
              pageFileName: 'blog',
              rank: 'U',
              description: '',
              imageUrl: '',
              repoUrl: ''
            },
            {
              displayName: 'Simple blog detail landing',
              slug: 'simple-blog-detail-landing',
              fileName: 'simple-blog-detail-landing',
              cmpName: 'SimpleBlogDetailLanding',
              pagePath: '/posts/',
              pageFileName: '[slug]',
              rank: 'U',
              description: '',
              imageUrl: '',
              repoUrl: ''
            }
          ]
        }
      ]
    }
  };
}

export default withRouter(Page);
