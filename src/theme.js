const theme = {
    colorPrimary: 'green',
    colorPrimaryContrast: '#fff',
    colorSecondary: 'orange',
    colorSecondaryContrast: '#fff',
    colorTertiary: 'green',
    colorTertiaryContrast: '#fff',
    colorBackground: '#fff',
    colorBackroundContrast: '#333',
    colorDark: '#333'
};

export default theme;