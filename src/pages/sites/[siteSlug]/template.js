import Head from 'next/head';
import { withRouter } from 'next/router';

const postMessage = async eventName => {
  parent.postMessage(eventName, '*');
};

const Page = ({ template }) => {
  return (
    <>
      <Head>
        <title>Template</title>
      </Head>
      <main className="style-tile">
        <h3>Style tile</h3>
        <div className="color-palette">
          <div
            className="color primary-color"
            onClick={() => postMessage('primaryColor')}
          ></div>
          <div
            className="color secondary-color"
            onClick={() => postMessage('secondaryColor')}
          ></div>
          <div
            className="color tertiary-color"
            onClick={() => postMessage('tertiaryColor')}
          ></div>
          <div
            className="color dark-color"
            onClick={() => postMessage('darkColor')}
          ></div>
        </div>
        <div className="buttons">
          <button className="btn btn-outline-primary">
            Secondary call to action
          </button>
          <button className="btn btn-primary">Main call to action</button>
        </div>
        <div style={{ display: 'flex' }}>
          <div className="headings">
            <h1>Heading 1</h1>
            <h2>Heading 2</h2>
            <h3>Heading 3</h3>
            <h4>Heading 4</h4>
            <h5>Heading 5</h5>
            <h6>Heading 6</h6>
            <p>Body font</p>
          </div>
          <div style={{ width: '60%' }}>
            <div className="primaryBg">
              <h2>Section title</h2>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec
                accumsan, nisi quis ullamcorper facilisis, lectus leo aliquet
                magna, sed blandit est ante vitae dui. Etiam volutpat
                pellentesque vehicula. Morbi blandit justo id auctor ornare.
                Nullam gravida eget lectus in laoreet.
              </p>
            </div>
          </div>
        </div>
      </main>
      <style jsx>{`
        .style-tile {
          padding: 20px;
        }
        .color-palette {
          display: flex;
          padding: 20px 0;
        }
        .color-palette .color {
          width: 100px;
          height: 100px;
          border-radius: 50%;
          border: 1px solid #ccc;
          margin-left: 10px;
        }
        .primary-color {
          background-color: var(--primary-color);
        }
        .secondary-color {
          background-color: var(--secondary-color);
        }
        .tertiary-color {
          background-color: var(--tertiary-color);
        }
        .dark-color {
          background-color: var(--dark-color);
        }
        .buttons {
          padding: 15px 0;
        }
        .buttons .btn-primary {
          background-color: var(--secondary-color);
          margin-left: 10px;
        }
        .buttons .btn-outline-primary {
          color: var(--secondary-color);
          border-color: var(--secondary-color);
        }
        .headings {
          padding: 15px 0;
          width: 40%;
        }
        .primaryBg {
          margin-top: 40px;
          padding: 10px 20px 20px 20px;
          overflow-x:hidden;
          background-color: var(--primary-color);
          color: var(--primary-contrast-color);
        }
      `}</style>
    </>
  );
};

export default withRouter(Page);
