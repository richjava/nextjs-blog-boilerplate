export function parseGlobalStyles(styles) {
  let parsedStyles = '\n';
  for (const cmpProp in styles) {
      switch (cmpProp) {
        case 'variables':
          for (const type in styles[cmpProp]) {
            parsedStyles += `html {\n`;
            for (const prop in styles[cmpProp][type]) {
              parsedStyles += `--${prop.replace(
                /[A-Z]/g,
                m => '-' + m.toLowerCase()
              )}: ${styles[cmpProp][type][prop]};\n`;
            }
            parsedStyles += `\n}`;
          }
          break;
        case 'classes':
          for (const classProp in styles[cmpProp]) {
            parsedStyles += `.${classProp.replace(
              /[A-Z]/g,
              m => '-' + m.toLowerCase()
            )} {\n`;
            for (const cssProp in styles[cmpProp][classProp]) {
              parsedStyles += `\t${cssProp.replace(
                /[A-Z]/g,
                m => '-' + m.toLowerCase()
              )}: ${styles[cmpProp][classProp][cssProp]};\n`;
            }
            parsedStyles += `}\n`;
          }
          break;
        case 'tags':
          for (const tagProp in styles[cmpProp]) {
            parsedStyles += `${tagProp} {\n`;
            for (const cssProp in styles[cmpProp][tagProp]) {
              parsedStyles += `\t${cssProp}: ${styles[cmpProp][tagProp][cssProp]};\n`;
            }
            parsedStyles += `\n}\n`;
          }
          break;
      }
  }
  return parsedStyles;
}
// export function parseGlobalStyles(style) {
//   let parsedStyles = '\n:root {\n';
//   for (const type in style) {
//     for (const prop in style[type]) {
//       parsedStyles += `--${prop.replace(
//         /[A-Z]/g,
//         m => '-' + m.toLowerCase()
//       )}: ${style[type][prop]};\n`;
//     }
//   }
//   parsedStyles += '\n}';
//   return parsedStyles;
// }

// export function parseStyles(styles) {
//     debugger
//   let parsedStyles = '\n';
//   for (const cmpProp in styles) {
//     if (cmpProp !== component.name)
//       switch (cmpProp) {
//         case 'classes':
//           for (const classProp in styles[cmpProp]) {
//             parsedStyles += `.${classProp.replace(
//               /[A-Z]/g,
//               m => '-' + m.toLowerCase()
//             )} {\n`;
//             for (const cssProp in styles[cmpProp][classProp]) {
//               parsedStyles += `\t${cssProp}: ${styles[cmpProp][classProp][cssProp]};\n`;
//             }
//             parsedStyles += `}\n`;
//           }
//           break;
//         case 'tags':
//           for (const tagProp in styles[cmpProp]) {
//             parsedStyles += `${tagProp} {\n`;
//             for (const cssProp in styles[cmpProp][tagProp]) {
//               parsedStyles += `\t${cssProp}: ${styles[cmpProp][tagProp][cssProp]};\n`;
//             }
//             parsedStyles += `\n}\n`;
//           }
//           break;
//         default:
//           return;
//       }
//   }
//   return parsedStyles;
// }
