import { Component, useState, useEffect } from "react";
import shortid from "shortid";
import Head from 'next/head';
import Link from 'next/link';
import dynamic from 'next/dynamic'
import { useRouter } from 'next/router'
import { withRouter } from 'next/router';
const fetchTemplateData = async () => {
    return [{
        "displayName": "Simple",
        "slug": "simple-contact-landing",
        "fileName": "simple-contact-landing",
        "cmpName": "SimpleContactLanding",
        "pageFileName": "contact",
        "repoUrl": ""
    },{
        "displayName": "Simple",
        "slug": "simple-home-landing",
        "fileName": "simple-home-landing",
        "cmpName": "SimpleHomeLanding",
        "pageFileName": "index",
        "repoUrl": "",
        "content": {
        }
    }]
}

const Page = ({ template }) => {
    let Template = dynamic(() => import(`../../../../../../components/${template.slug}`))
    return (
        <>
            <Head>
                <title>Template</title>
            </Head>
            <main>
                <Template />
            </main>
            <style jsx>{`
    `}</style>
        </>
    );
};

export async function getServerSideProps({query}) {
    const templateSlug = query.template;
    const data = await fetchTemplateData();
    let template;
    for (let i = 0; i < data.length; i++) {
        const t = data[i];
        if(t.slug === templateSlug){
            template = t;
            break;
        }
    }
    return {
        props: { template: template }
    };

}

export default withRouter(Page);